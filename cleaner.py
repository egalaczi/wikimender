import re

def R(regex):
    return lambda data: re.compile(regex, flags=re.DOTALL).sub('', data)

def clean(text):
    rules = [
        R(r'{{.*?}}'),
        R(r'{\|.*?\|}'),
        #remove html tags
        R(r'<.*?>'),
        #remove hyperlinks inside []
        R(r'\[(http://.*?)\]'),
        lambda data: re.compile(r'=([a-zA-Z].*[a-zA-Z])=').sub(r'\1', data),
        lambda data: re.compile(r'\[\[(?:[^|\]]*\|)?([^\]]+)\]\]').sub(r'\1', data),
        
    ]
    for rule in rules:
        text = rule(text)

    return text.replace("=====","").\
        replace("====","").\
        replace("===","").\
        replace("==","").\
        replace("\'\'","").\
        replace("\'\'\'","")

def main(sourceFileName):
  MYFILE = "temp.txt"
  source = open(sourceFileName)
  text = source.read()
  dest = open("temp.out.txt","w")
  dest.write(clean(text))
  #wikilink_rx = re.compile(r'\[\[(?:[^|\]]*\|)?([^\]]+)\]\]')
  #dest.write(wikilink_rx.sub(r'\1', text))

  dest.close()
if __name__ == "__main__":
  main(MYFILE)