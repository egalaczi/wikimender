import operator
import pysolr


def getMoreLikeQuery(sI, text):
    params = {
        'q': text,
        'mlt': 'true',
        'mlt.fl': 'manu,cat',
        'mlt.mindf': 1,
        'mlt.mintf': 1,
        'fl': 'id,score,name',
        'mlt.count': 4,
        'mlt:qf': 'text^0.5 features^1.0 name^1.2 sku^1.5 id^10.0 manu^1.1 cat^1.4 '
                  'title^10.0 description^5.0 keywords^5.0 author^2.0 resourcename^1.0'
    }

    response = sI._select(params)

    result = sI.decoder.decode(response)

    if result.get('moreLikeThis'):
        moreLikeThis = result['moreLikeThis']
    else:
        moreLikeThis = {}

    if 'QTime' in result.get('responseHeader', {}):
        qTime = result['responseHeader']['QTime']

    response = result.get('response') or {}
    return response.get('docs'), moreLikeThis, qTime


if __name__ == "__main__":
    sI = pysolr.Solr('http://localhost:8983/solr')
    results, moreLikeThis, qTime = getMoreLikeQuery(sI,
                                                    text='NFL')
    top = dict()
    for result in iter(moreLikeThis):
        for found in iter(moreLikeThis[result][u'docs']):
            if found[u'id'] not in top or top[found[u'id']][u'score'] < found[u'score']:
                top[found[u'id']] = found

    sorted_x = sorted(top.values(), key=lambda item: item[u'id'])
    for s in sorted_x:
        print s
