from datetime import datetime
import random
import re
import simplejson
import string
import urllib
from progressbar import Percentage, Bar, ETA, ProgressBar, ReverseBar, Counter
from pymongo import MongoClient
import json
import HTMLParser
import types
from restkit import Connection, Resource
from socketpool import ConnectionPool
import pysolr
from sphinx.builders.linkcheck import opener
import stopwatch
import sys
import lev

class OpenLibrary(Resource):
    API_URL = "http://openlibrary.org/api/"
    _pool = ConnectionPool(factory=Connection, max_size=10)

    def __init__(self, pool_instance=None, **kwargs):
        super(OpenLibrary, self).__init__(OpenLibrary.API_URL, follow_redirect=True,
                                          max_follow_redirect=10,
                                          pool=OpenLibrary._pool)

    def forISBN(self, isbn):
        return self.get(path='books', bibkeys="ISBN:%s" % isbn, jscmd="data", format="json");

    def request(self, *args, **kwargs):
        resp = super(OpenLibrary, self).request(*args, **kwargs)
        return json.loads(resp.body_string('UTF-8'))

    def extract(self, answer):
        return json.loads(answer)


class GoogleBooks(Resource):
    API_URL = "https://www.googleapis.com/books/v1/"
    KEY = "AIzaSyDxt6e3cCPOgCcH_Ey6Cu0ckxehu0J7r1o"
    _pool = ConnectionPool(factory=Connection, max_size=10)

    def __init__(self, pool_instance=None, **kwargs):
        super(GoogleBooks, self).__init__(GoogleBooks.API_URL, follow_redirect=True,
                                          max_follow_redirect=10,
                                          pool=OpenLibrary._pool)

    def getSimilarByText(self, text):
        return self.get(path='volumes', q=text, key=GoogleBooks.KEY)

    def request(self, *args, **kwargs):
        resp = super(GoogleBooks, self).request(*args, **kwargs)
        body = resp.body_string()
        return json.loads(body)

    def getWithId(self, id):
        pathStr = 'volumes/%s' % id
        return self.get(path=pathStr, key=GoogleBooks.KEY)

    def extract(self, answer):
        return json.loads(answer)


def add(document, atDocument, item, inside, key, htmlParser, collapse=None):
    encode = None
    if inside in item and key in item[inside]:
        if isinstance(item[inside][key], types.StringTypes):
            document[atDocument] = htmlParser.unescape(
                item[inside][key].encode(encode) if encode else item[inside][key])
        else:
            if collapse:
                document[atDocument] = htmlParser.unescape(
                    ' '.join([a.encode(encode) if encode else a for a in item[inside][key]]))
            else:
                document[atDocument] = [htmlParser.unescape(a.encode(encode) if encode else a) for a in
                                        item[inside][key]]

#isbn-> book metadata
def collect():
    O = OpenLibrary()
    G = GoogleBooks()
    h = HTMLParser.HTMLParser()
    sI = pysolr.Solr('http://localhost:8983/solr')

    isbns = [line.strip() for line in open('ISBN_10M.txt')]
    at = 0
    found = 0
    all = len(isbns)
    print all
    with open('ISBN_NOT_FOUND.txt', 'a', 0) as notFound:
        for isbn in isbns:
            at += 1
            if at <= 63464:
                continue
            t = stopwatch.Timer()
            ok = False
            try:
                oR = O.forISBN(isbn)
                isbnText = "ISBN:%s" % isbn
            except Exception as e:
                print e.message
                isbnText = None
            if isbnText in oR and u'title' in oR[isbnText] and oR[isbnText][u'title']:
                googleQuery = "%s %s" % (oR[isbnText][u'title'],
                                         " ".join(a[u"name"] for a in oR[isbnText][u"authors"]) if u'authors' in oR[
                                             isbnText] else "")
                try:
                    gR = G.getSimilarByText(googleQuery)
                except Exception as e:
                    gr = {}
                    print e.message
                result = []
                if u'items' in gR:
                    for item in gR[u'items']:
                        document = {'id': item[u'id'],
                                    'category': item[u'kind']}
                        add(document, 'cat', item, u'volumeInfo', u'categories', h)
                        add(document, 'name', item, u'volumeInfo', u'title', h)
                        add(document, 'author_t', item, u'volumeInfo', u'authors', h, True)
                        add(document, 'description', item, u'volumeInfo', u'description', h)
                        add(document, 'subject', item, u'volumeInfo', u'subtitle', h)
                        add(document, 'content', item, u'volumeInfo', u'infoLink', h)
                        add(document, 'comments', item, u'searchInfo', u'textSnippet', h)

                        if u'volumeInfo' in item and u'industryIdentifiers' in item[u'volumeInfo']:
                            for ident in item[u'volumeInfo'][u'industryIdentifiers']:
                                if ident[u'type'] == "ISBN_10":
                                    document['manu'] = ident[u'identifier']
                                if ident[u'type'] == "ISBN_13":
                                    document['sku'] = ident[u'identifier']
                        result.append(document)
                        found += 1
                        ok = True
                if result:
                    try:
                        sI.add(result)
                    except Exception as e:
                        ok = False
                        print e.message

            if not ok:
                notFound.write("%s\n" % isbn)

            print "%s %s/%s (%s) %s %s ~ %s -> %s" % (
                str(datetime.today()), at, all, found, "OK" if ok else "NO", oR[isbnText][u'title'] if ok else "-",
                str(t), 100.0 * at / all)
            t.stop()


#All document for db 65709,  with ISBN list 12358, where isbn10 46332, isbn13 14008 and other 0
#Solr: isbn10: 20757, isbn13 6324, except count 0
# db.valid_articles.count() 8480

def cleanInvalidISBN():
    sI = pysolr.Solr('http://localhost:8983/solr')

    connection = MongoClient()
    db = connection.test_database
    articles = db.articles
    filtered = db.valid_articles

    all = articles.count()
    widgets = [Bar('>'), ' ', ETA(), ' ', ReverseBar('<'), Counter(), ' ', Percentage()]
    pbar = ProgressBar(widgets=widgets, maxval=all)
    pbar.start()

    at = 0
    skipped = 0
    count = 0
    isbn13_count = 0
    isbn13_count_found = 0
    isbn10_count = 0
    isbn10_count_found = 0
    isbn_invalid_count = 0
    isbn13 = open("ISBN13_DB", 'w')
    isbn10 = open("ISBN10_DB", 'w')
    isbn13_found = open("ISBN13_DB_FOUND", 'w')
    isbn10_found = open("ISBN10_DB_FOUND", 'w')
    except_count = 0
    for article in articles.find():
        at += 1
        pbar.update(at)

        if not article[u'isbn_list']:
            skipped += 1
            continue
        isbnList = []
        for isbn in article[u'isbn_list']:
            if len(isbn) == 13:
                isbn13_count += 1
                try:
                    res = sI.search('sku:%s' % isbn)
                except:
                    except_count += 1
                    continue
                isbn13.write("%s\n" % isbn)
                if len(res) is not 0:
                    isbn13_count_found += 1
                    isbnList.append(isbn)
                    isbn13_found.write("%s\n" % isbn)
            else:
                if len(isbn) == 10:
                    isbn10_count += 1
                    try:
                        res = sI.search('manu:%s' % isbn)
                    except:
                        except_count += 1
                        continue
                    isbn10.write("%s\n" % isbn)
                    if len(res) is not 0:
                        isbn10_count_found += 1
                        isbnList.append(isbn)
                        isbn10_found.write("%s\n" % isbn)
                else:
                    isbn_invalid_count += 1
        if len(isbnList) > 0:
            article[u'isbn_list'] = isbnList
            del article[u'_id']
            filtered.insert(article)
    print "All document for db %s,  with ISBN list %s, where isbn10 %s, isbn13 %s and other %s\nSolr: isbn10: %s, isbn13 %s, except count %s" % \
          (all, all - skipped, isbn10_count, isbn13_count, isbn_invalid_count, isbn10_count_found,
           isbn13_count_found, except_count)


def evaluation():
    sI = pysolr.Solr('http://localhost:8983/solr')

    connection = MongoClient()
    db = connection.test_database
    articles = db.valid_articles

    

    all = articles.count()
    widgets = [Bar('>'), ' ', ETA(), ' ', ReverseBar('<'), Counter(), ' ', Percentage()]
    pbar = ProgressBar(widgets=widgets, maxval=all)
    pbar.start()
    at = 0
    recall = []
    precision = []
    atMeasure = 100
    for article in articles.find():
        at += 1
        pbar.update(at)

        #article[u'text']         article[u'isbn_list']        article[u'title']
        query = filter(lambda x: x in string.printable, article[u'text'])
        query = re.sub('[:)(}{;\n"\'\*\[\]\+\-]', ' ', query)
        query = re.sub('&nbsp', ' ', query)
        query = re.sub('[ ]+', ' ', query)
        isbnList = set()
        for isbn in article[u'isbn_list']:
            isbnList.add(isbn)
        union = len(isbnList)
        interSection = 0
        try:
            id = str(int(random.random() * 10e15))
            document = {'id': id,
                        'name': article[u'title'],
                        'description': query}
            sI.add([document])
            # http://wiki.apache.org/solr/MoreLikeThis
            #http://localhost:8983/solr/select?q=id:Anarchism&mlt=true&mlt.fl=manu,cat,description&mlt.mindf=1&mlt.mintf=1&fl=id,score,name,author_t,&wt=json&mlt.count=100
            f = opener.open(
                "http://localhost:8983/solr/select?q=id:%s&mlt=true&mlt.fl=title,description&mlt.mindf=1&mlt.mintf=5&"
                "fl=id,score,name,author_t,sku,manu&wt=json&mlt.count=%d" % (id, atMeasure))
            result = simplejson.load(f)
            for s in result[u'moreLikeThis'][id][u'docs']:
                isbn10 = unicode(s[u'manu'] if u'manu' in s else '')
                isbn13 = unicode(s[u'sku'] if u'sku' in s else '')
                #print  isbn13, isbn10 , '=>', isbnList
                if isbn13 in isbnList or isbn10 in isbnList:
                    interSection += 1
                else:
                    union += 1
            sI.delete(id=article[u'title'])
        except pysolr.SolrError as e:
            print e.message
        recall.append(1.0 * interSection / len(isbnList))
        precision.append(1.0 * interSection / atMeasure)
    print '\nRecall', sum(recall) / len(recall)
    print 'Precision', sum(precision) / len(precision)

def evaluation2():
    starttime = datetime.now()

    sI = pysolr.Solr('http://localhost:8983/solr')

    connection = MongoClient()
    db = connection.test_database
    articles = db.valid_articles

    LIMIT = 8480
    #all = articles.count() 
    all=LIMIT
    thresh = 0.01

    widgets = [Bar('>'), ' ', ETA(), ' ', ReverseBar('<'), Counter(), ' ', Percentage()]
    pbar = ProgressBar(widgets=widgets, maxval=all)
    pbar.start()
    at = 0
    recall = []
    precision = []
    #more like this count
    atMeasure = 100
    
    for article in articles.find()[:LIMIT]:
        at += 1
        if random.random() > thresh:
            continue
        
        pbar.update(at)
        
        #get names of books in article
        booknames = []
        for isbn in article['isbn_list']:
            #import ipdb;ipdb.set_trace()
            if len(isbn) == 10:
                key = 'manu'
            else:
                key = 'sku'
            f = opener.open(
                "http://localhost:8983/solr/select?q=%s:%s&wt=json" % (key, isbn))
            result = simplejson.load(f)

            booknames.append(result['response']['docs'][0]['name'])

        

        #clean article text for query
        query = filter(lambda x: x in string.printable, article[u'text'])
        query = re.sub('[:)(}{;\n"\'\*\[\]\+\-]', ' ', query)
        query = re.sub('&nbsp', ' ', query)
        query = re.sub('[ ]+', ' ', query)

        resultnames = []
        try:
            id = str(int(random.random() * 10e15))
            document = {'id': id,
                        'name': article[u'title'],
                        'description': query}
            sI.add([document])
            
            f = opener.open(
                "http://localhost:8983/solr/select?q=id:%s&mlt=true&mlt.fl=title,description&mlt.mindf=1&mlt.mintf=5&"
                "fl=id,score,name,author_t,sku,manu&wt=json&mlt.count=%d" % (id, atMeasure))
            result = simplejson.load(f)
            

            for s in result[u'moreLikeThis'][id][u'docs']: resultnames.append(s['name']) 

            sI.delete(id=article[u'title'])

        except pysolr.SolrError as e:
            print e.message
        
        limit = 4
        match = 0
        for i in booknames:
            match2 = 0
            for j in resultnames:
                if lev.levenshtein(i,j) < (len(i) / 2) : match2+=1
            if match2: match+=1


        recall.append(1.0 * match / len(booknames))
        precision.append(1.0 * match / atMeasure)

    print '\nRecall', sum(recall) / len(recall)
    print 'Precision', sum(precision) / len(precision)

    print "Time:", datetime.now() - starttime, "count:", len(precision)

if __name__ == "__main__":
    evaluation2()
