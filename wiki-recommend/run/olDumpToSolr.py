import csv
import json
import simplejson
import sys

import pysolr


def look(D, J):
    if not isinstance(J, dict):
        return
    for i in J:
        if i not in D:
            D[i] = {}
        look(D[i], J[i])


def getJSONStructure(fileName):
    #_csv.Error: field larger than field limit (131072)
    csv.field_size_limit(sys.maxsize)
    D = dict()
    at = 0
    with open(fileName, 'rb') as dumpFile:
        tsvReader = csv.reader(dumpFile, delimiter='\t')
        for row in tsvReader:
            type = row[0]
            key = row[1]
            revisionNr = row[2]
            last_modified = row[3]
            jsonInformatin = row[4]
            jsonInformatin = jsonInformatin.replace('\\r', '')
            jsonInformatin = jsonInformatin.replace('\\n', ' ')
            try:
                jsonS = simplejson.loads(jsonInformatin)
                if type not in D:
                    D[type] = {}
                look(D[type], jsonS)
            except:
                pass
            at += 1
            if at % 1000000 is 0:
                print json.dumps(D, indent=2)
                print at
    print json.dumps(D, indent=2)


if __name__ == "__main__":
    getJSONStructure('/data/univ/ol_dump_2013-01-31.txt')
    sI = pysolr.Solr('http://localhost:8984/solr')





