wikimender
==========

Install mongodb

http://docs.mongodb.org/manual/installation/

run mongodb

`mongod`

Set up your environment:

`virtualenv .env`

`source .env/bin/activate`

`pip install -r requirements.pip`

Running:

`python importer.py <pathtoxmlfile>`

`python util/query.py Anarchism`