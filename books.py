import sys
from pymongo import MongoClient
connection = MongoClient()
db = connection.test_database
books = db.books
import json
import re
f = open(sys.argv[1])

db.books.remove()

#use this instead of f.readlines() because it is memory efficient
for line in f:
    jsin = line.split("\t")[-1]


    jsin_temp = jsin.replace('\\"','XXTEMPORARYESCAPEXX')
    jsin_temp = jsin_temp.decode('unicode_escape').encode('utf-8')
    jsin = jsin.replace('XXTEMPORARYESCAPEXX', '\\"')
    try:
        books.insert(json.loads(jsin))
    except Exception, e:
        print e
        pass

#querying
import ipdb;ipdb.set_trace()
db.books.find({'type.key':'/type/work'})
db.books.find({'type.key':'/type/book'})
db.books.find({'type.key':'/type/edition'})


