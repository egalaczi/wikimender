import xml.sax
import os,sys


from cleaner import clean
from isbn import findisbn
def update_progress(progress):
    sys.stdout.write ('\r[{0}{1}] {2}%'.format(
        '#'*(progress/2),
        '.'*(50-(progress/2)),
         progress) )
    sys.stdout.flush()

class WikiContentHandler(xml.sax.ContentHandler):
    def __init__(self, length, articles):
        xml.sax.ContentHandler.__init__(self)
        self.inside_text_tag = False
        self.inside_title_tag = False
        self.text_content = []
        self.length = int(length)
        self.articles = articles




        
    def startElement(self, name, attrs):
        if name == 'text':
            self.inside_text_tag = True
        elif name == 'title':
            self.inside_title_tag = True

    def endElement(self, name):
        if name == 'text':
            self.inside_text_tag = False
            self.text = ''.join(self.text_content)
            progress = 100 * self._locator.getLineNumber() 
            update_progress(progress / self.length + 1)

            cleaned = clean(self.text)
            
            #if len(self.text_content) > 100 and len(self.text_content) < 1000 and not self.temp:
            #    self.temp = True
            #    f = open("temp.txt","w")
            #    [ f.write(x.encode('ascii', 'replace')) for x in self.text_content ] 
            #    f.close()

            self.text_content = []
            
            
            article = {
                "title": self.title,
                "text": cleaned,
                "isbn_list":findisbn(cleaned)
            }
            self.articles.insert(article)
        elif name == 'title':
            self.inside_title_tag = False




    def characters(self, content):        
        if self.inside_text_tag:
            self.text_content.append(content)
        elif self.inside_title_tag:
            self.title = content
            #print content


def main(sourceFileName, args=sys.argv):
  source = open(sourceFileName)
  print "Initializing..."

  print "Setting up db"
  from pymongo import MongoClient
  connection = MongoClient()
  db = connection.test_database
  articles = db.articles

  print "Getting initial information..."
  if "noprogress" in args:
    length = 10000000
  else:
    length = os.popen("cat %s | wc -l"% sys.argv[1]).read()
  print "Starting..."

  try:xml.sax.parse(source, WikiContentHandler(length = length, articles = articles))
  except Exception, e: print e

  print "Finished..."
 
if __name__ == "__main__":
  main(sys.argv[1])
