import re

def findisbn(text):
    matches = re.findall(r'ISBN[ =]+[0-9- \X]*', text, re.IGNORECASE)
    isbn_list = map(
        lambda x: x.replace("ISBN","").replace("isbn","")
        ,matches)
    isbn_list = map(
        lambda x:
        re.compile(r"[ =-]").sub("", x )
        ,isbn_list)
    return filter(lambda x: len(x) == 10 or len(x) == 13, isbn_list)
    